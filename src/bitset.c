
#include <bitset.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>

#define WORD_BITS (CHAR_BIT * sizeof(bitset_container_t))

int bitset_init(bitset* set, size_t size) {
    if (set == NULL) {
        return -1;
    }

    // check for possible integer overflow
    if (size == 0) {
        return -2;
    }

    size_t numint = (size / WORD_BITS);
    if ( 0 != (size % WORD_BITS) ) {
        numint += 1;
    }

    if (
        numint > SIZE_MAX / sizeof(bitset_container_t)
    ) {
        // case of (size / WORD_BITS + 1) * sizeof(bitset_container_t) has a integer overflow:
        // can not happen, size has a integer overflow before calling this function
        // before this expression could overflow
        // case is still catched to be safe, that unknown behaviour happens!
        return -3;
    }

    // init bitset
    *set = (bitset) {
        .container = calloc(numint, sizeof(bitset_container_t)),
        .size = size
    };

    // check for error with malloc
    if (set->container == NULL) {
        set->size = 0;
        errno = 0;
        return -4;
    }

    return 0;
}

void bitset_destroy(bitset* set) {
    if (set == NULL) {
        return;
    }

    if (set->container != NULL) {
        free(set->container);
        set->container = NULL;
    }

    if (set->size != 0) {
        set->size = 0;
    }
}

int bitset_get_bit(bitset* bits, size_t pos) {
    if (bits == NULL || bits->container == NULL || bits->size == 0) {
        return -1;
    }

    if (pos >= bits->size) {
        return -2;
    }

    size_t index = pos / WORD_BITS;
    size_t rel_index = pos % WORD_BITS;

    return (bits->container[index] >> rel_index) & 1;
}

int bitset_set_bit(bitset* bits, size_t pos) {
    if (bits == NULL || bits->container == NULL || bits->size == 0) {
        return -1;
    }

    if (pos >= bits->size) {
        return -2;
    }

    int index = pos / WORD_BITS;
    int rel_index = pos % WORD_BITS;

    bits->container[index] |= 1 << rel_index;
    return 0;
}

int bitset_set_all_bits(bitset* bits) {
    if (bits == NULL || bits->container == NULL || bits->size == 0) {
        return -1;
    }

    int last_index = bits->size / WORD_BITS;    // get last index
    int rel_index = bits->size % WORD_BITS;     // get relative position of last bit in 

    for (size_t i = 0; i < last_index+1; i++) {
        if (i==last_index) {
            bits->container[i] |= (~0) >> (WORD_BITS-rel_index);
        } else {
            bits->container[i] |= ~0;
        }
    }

    return 0;
}

int bitset_unset_bit(bitset* bits, size_t pos) {
    if (bits == NULL || bits->container == NULL || bits->size == 0) {
        return -1;
    }

    if (pos >= bits->size) {
        return -2;
    }

    size_t index = pos / WORD_BITS;
    size_t rel_index = pos % WORD_BITS;

    bits->container[index] &= ~(1 << rel_index);
    return 0;
}
