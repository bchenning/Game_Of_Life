#include <ncurses.h>

int init() {
	initscr();	/* start ncurses mode */

	return 0;
}

int main_loop() {
	while (true) {
		printw("Hello World !!!");
		refresh();
		getch();
	}
}

void end() {
	endwin();	/* stop ncurses mode */
}

int main(void) {

	init();
	main_loop();
	end();

	return 0;
}
