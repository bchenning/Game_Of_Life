
#ifndef _BITSET_H_
#define _BITSET_H_ 1

#include <stddef.h>
#include <stdbool.h>

typedef unsigned int bitset_container_t;

/**
 * bitset - a bit set with a constant length
 *
 * bitset can have a possible internal fragmentation of maximal sizeof(unsigned int)-1
 * bits that are not used by the bit set, but are allocated
 */
typedef struct {
	bitset_container_t *container;
	size_t size;
} bitset;

/**
 * bitset_init() - initialises a bitset at the address with _size_ bits size
 *
 * bitset_init() initialises a bitset with _size_ bits, these bits are all zero
 * after initialisation. This bitset can store up to _size_ bits.
 * The bitset_init()-function returns zero on success.
 * On error, this function returns -1 and writes an error code to **errno**.
 *
 * If _size_ is zero, the bitset is not initialised and this function returns -2
 *
 * @param[in,out]	bits	pointer to a bit set that should be initialised with this function
 * @param[in]		size	number of bits that the bitset can store
 *
 * @return	On success this function returns zero.
 * 			On error this function returns -1, if bits is _NULL_
 * 										   -2, if _size_ is zero and
 * 										   -3, if the data structure can not handle a bit set of _size_ bits
 * 										   -4, if the system can not allocate enough memory for the data structure
 *
 */
int bitset_init(bitset *bits, size_t size);

/**
 * bitset_destroy() - destroys the given bitset
 *
 * bitset_destroy() destroys and deinitialises the given bit set and frees allocated
 * memory. If _bits_ is not initialised or _NULL_, no operation is performed.
 *
 * @param[in,out]	set		pointer to a bit set that should be destroyed with this function
 */
void bitset_destroy(bitset* set);

/**
 * bitset_get_bit() - returns the value of the bit with position _pos_
 *
 * bitset_get_bit() returns the value of the bit at the position _pos_
 *
 * @param[in]	bits	pointer to a bit set
 * @param[in]	pos		position of the bit that should be returned
 *
 * @return	On success, returns the value of the bit in the bit set at _pos_
 * 				as an int
 * 			On error, returns -1, if the _bits_ is _NULL_ or not initialised properly,
 * 			                  -2, if _pos_ is out of the range of the given bit set
 */
int bitset_get_bit(bitset* bits, size_t pos);

/**
 * bitset_set_bit() - sets a single bit in the bit set at _pos_
 *
 * bitset_set_bit() sets a single bit in the bit set _bits_ at the position _pos_
 *
 * @param[in,out]	bits	pointer to a bit set
 * @param[in]		pos		position of the bit that should be set
 *
 * @return	On success, return 0
 * 			On error, returns -1, if the _bits_ is _NULL_ or is not initialised properly,
 * 			                  -2, if _pos_ is out of the range of the given bit set
 */
int bitset_set_bit(bitset* bits, size_t pos);

/**
 * bitset_unset_bit() - unsets a single bit in the bit set at _pos_
 *
 * bitset_unset_bit() unsets a single bit in the bit set _bits_ at the position _pos_
 *
 * @param[in,out]	bits	pointer to a bit set
 * @param[in]		pos		position of the bit that should be unset
 *
 * @return	On success, return 0
 * 			On error, returns -1, if the _bits_ is _NULL_ or not initialised properly,
 * 			                  -2, if _pos_ is out of the range of the given bit set
 */
int bitset_unset_bit(bitset* bits, size_t pos);

/**
 * bitset_set_all_bits() - sets all bits in the bit set
 *
 * bitset_set_all_bits() sets all bits in the given bit set.
 *
 * @param[in,out]	bits	pointer to a bit set
 *
 * @return	On success, return 0
 *			On error, returns -1, if the _bits_ is _NULL_ or not initialised properly
 */
int bitset_set_all_bits(bitset* bits);

#endif
