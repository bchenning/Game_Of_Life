# Game of Life
This project is a simple implementation of [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Music) used inside of a terminal.

# Features
- [ ] input file as 1st generation
- [ ] use ncurses to create the user interface inside of the terminal, where the program was started
- [ ] use config files (YAML or JSON) for configuration of simulations:
	- [ ] dimensions of universe
	- [ ] generations between screen refreshs
