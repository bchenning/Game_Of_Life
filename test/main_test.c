
#include "bitset_test.h"

#include <CUnit/CUnitCI.h>

int main(int argc, char **argv) {
    CUNIT_CI_CLEAR_SETUPS();
    CU_CI_DEFINE_SUITE("first suite", 0, 0, 0, 0);
    CUNIT_CI_TEST(test_bitset_init);
    CUNIT_CI_TEST(test_bitset_destroy);
    CUNIT_CI_TEST(test_bitset_get_bit);
    CUNIT_CI_TEST(test_bitset_set_bit);
    CUNIT_CI_TEST(test_bitset_set_all_bits);
    CUNIT_CI_TEST(test_bitset_unset_bit);

    return CU_CI_RUN_SUITES();
}
