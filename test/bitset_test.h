
#ifndef _BITSET_TEST_H_
#define _BITSET_TEST_H_ 1

// 4002 asserts
void test_bitset_init(void);

// 1003 asserts
void test_bitset_destroy(void);

// 1004 asserts
void test_bitset_get_bit(void);

// 1006 asserts
void test_bitset_set_bit(void);

// 1003 asserts
void test_bitset_set_all_bits(void);

// 1004 asserts
void test_bitset_unset_bit(void);

#endif
