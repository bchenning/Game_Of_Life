
#include <CUnit/CUnitCI.h>

#include "bitset_test.h"
#include <bitset.h>

#include <stdint.h>
#include <limits.h>
#include <stdlib.h>
#include <time.h>

bitset testset;

// 2+4000 asserts
void test_bitset_init(void) {
    CU_ASSERT_EQUAL(-1, bitset_init(NULL, 5));

    int r = bitset_init(&testset, 0);
    if (    -2 == r &&
            NULL == testset.container &&
            0 == testset.size) {
        CU_PASS("bitset_init(<valid pointer to a bit set>, <size==0>): Successfully detected and handled invalid parameter <size==0>");
    } else {
        CU_FAIL("bitset_init(<valid pointer to a bit set>, <size==0>): Failed detecting or handling invalid parameter <size==0>");
    }
    if (NULL != testset.container) {
        free(testset.container);
    }
    testset.size = 0;

    // -3 and -4 as return values can't properly be test

    srand(time(NULL));
    for (size_t i = 0; i < 1000; i++) {
        size_t size = rand() % (1024*1024*1024-1) + 1;
        bitset_init(&testset, size);
        CU_ASSERT_EQUAL(0, bitset_init(&testset, size));
        CU_ASSERT_PTR_NOT_NULL(testset.container);
        bool correct = true;
        if (NULL != testset.container) {
            size_t last_index = (size-1) / (8*sizeof(*testset.container));
            for (size_t index; index < last_index+1; index++) {
                correct = correct && (0 == testset.container[index]);
            }
            free(testset.container);
        }
        CU_ASSERT_TRUE(correct);
        CU_ASSERT_EQUAL(size, testset.size);
    }
}

// 3+1000 asserts
void test_bitset_destroy(void) {
    // check for difficulties with NULL pointer as parameter
    bitset_destroy(NULL);
    CU_PASS("bitset_destroy(NULL): Returned without any problems");

    // check properly destruction of partly initialised bit set (only container)
    testset.container = malloc(sizeof(testset.container));
    testset.size = 0;
    bitset_destroy(&testset);
    if (testset.container == NULL && testset.size == 0) {
        CU_PASS("bitset_destroy(<only container initialised bit set>): Successfully destroyed");
    } else {
        CU_FAIL("bitset_destroy(<only container initialised bit set>: Failed to destroy)");
        free(testset.container);
    }

    // check properly destruction of partly initialised bit set (only size)
    testset.container = NULL;
    testset.size = 10;
    bitset_destroy(&testset);
    if (testset.container == NULL && testset.size == 0) {
        CU_PASS("bitset_destroy(<only size initialised bit set>): Successfully destroyed");
    } else {
        CU_FAIL("bitset_destroy(<only size initialised bit set>: Failed to destroy)");
        free(testset.container);
    }

    size_t size;
    for (size_t i = 2; i < 1000; i++) {
        // check for apropriate destruction of a complete initialised bit set
        size = ((size_t) rand()) % (1024*1024*1024-1) + 1;
        bitset_init(&testset, size);
        bitset_destroy(&testset);
        if (testset.container == NULL && testset.size == 0) {
            CU_PASS("bitset_destroy(<initialised bit set>): Successfully destroyed");
        } else {
            CU_FAIL("bitset_destroy(<initialised bit set>): Failed to destroy");
            free(testset.container);
        }
    }
}

// 4+1000 asserts
void test_bitset_get_bit(void) {
    // check for handling of NULL pointer as bit set
    CU_ASSERT_EQUAL_FATAL(-1, bitset_get_bit(NULL, 0));

    // check for handling of a not properly initialised bit set
    testset.container = malloc(5*sizeof(bitset_container_t));
    testset.size = 0;
    CU_ASSERT_EQUAL_FATAL(-1, bitset_get_bit(&testset, 0));

    testset.container = NULL;
    testset.size = 5;
    CU_ASSERT_EQUAL_FATAL(-1, bitset_get_bit(&testset, 0));
    bitset_destroy(&testset);

    // check for out of range handling as position
    bitset_init(&testset, 1024*1024);
    CU_ASSERT_EQUAL_FATAL(-2, bitset_get_bit(&testset, 1024*1024));
    bitset_destroy(&testset);

    // check for correct handling of correct bits gotten
    srand(time(NULL));
    size_t size = 128;
    bitset_init(&testset, size);

    size_t last_index = (size-1) / (8*sizeof(*testset.container));
    size_t last_rel_index = (size-1) % (8*sizeof(*testset.container));

    testset.container[0] = 1;
    CU_ASSERT_EQUAL_FATAL(1, bitset_get_bit(&testset, 0));
    testset.container[0] = 0;

    testset.container[last_index] = 1 << last_rel_index;
    CU_ASSERT_EQUAL_FATAL(1, bitset_get_bit(&testset, size-1));
    testset.container[last_index] = 0;

    for (size_t i = 0; i<1000; i++) {
        size_t pos = rand() % size;
        size_t index = pos / (8*sizeof(*testset.container));
        size_t rel_index = pos % (8*sizeof(*testset.container));
        testset.container[index] = 1 << rel_index;
        CU_ASSERT_EQUAL_FATAL(1, bitset_get_bit(&testset, pos));
        testset.container[index] = 0;
    }

    bitset_destroy(&testset);
}

// 6+1000
void test_bitset_set_bit(void) {
    // check for correct handling of a NULL pointer instead of a bit set
    CU_ASSERT_EQUAL_FATAL(-1, bitset_set_bit(NULL, 10));

    // check for correct handling of a not properly initialised bit set
    testset.container = NULL;
    testset.size = 20;
    CU_ASSERT_EQUAL_FATAL(-1, bitset_set_bit(&testset, 10));

    testset.container = malloc(10*sizeof(bitset_container_t));
    testset.size = 0;
    CU_ASSERT_EQUAL_FATAL(-1, bitset_set_bit(&testset, 0));
    bitset_destroy(&testset);

    // check for correct handling of a position out of range
    bitset_init(&testset, 1024);
    CU_ASSERT_EQUAL_FATAL(-2, bitset_set_bit(&testset, 1024));
    bitset_destroy(&testset);

    // check for setting only the desired bit
    {
        size_t size = 1024;
        size_t pos = size-1;
        bitset_init(&testset, size);

        size_t index = pos / (8*sizeof(bitset_container_t));
        size_t rel_index = pos % (8*sizeof(bitset_container_t));
        bitset_container_t number = ((bitset_container_t) 1 << rel_index);

        int r = bitset_set_bit(&testset, pos);
        if (r != 0) {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed while setting the bit.");
        }

        CU_ASSERT_EQUAL_FATAL(1, bitset_get_bit(&testset, pos));

        bool correct = true;
        for (size_t i = 0; i<index+1; i++) {
            if (i == index) {
                correct = correct && (testset.container[i] == number);
            } else {
                correct = correct && (testset.container[i] == 0);
            }
        }
        if (correct == true) {
            CU_PASS("bitset_set_bit(<initialised bit set>,<position in range>): Successfully set only the desired bit.");
        } else {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed to set only the desired bit.");
        }
        bitset_destroy(&testset);
    }

    { 
        size_t size = 1024;
        size_t pos = 0;
        bitset_init(&testset, size);
        size_t index = pos / (8*sizeof(bitset_container_t));
        size_t rel_index = pos % (8*sizeof(bitset_container_t));
        bitset_container_t number = ((bitset_container_t) 1 << rel_index);

        int r = bitset_set_bit(&testset, pos);
        if (0 != r) {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed while setting the bit.");
        }

        CU_ASSERT_EQUAL_FATAL(1, bitset_get_bit(&testset, pos));

        bool correct = true;
        for (size_t i = 0; i<index+1; i++) {
            if (i == index) {
                correct = correct && (testset.container[i] == number);
            } else {
                correct = correct && (testset.container[i] == 0);
            }
        }
        if (correct == true) {
            CU_PASS("bitset_set_bit(<initialised bit set>,<position in range>): Successfully set only the desired bit.");
        } else {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed to set only the desired bit.");
        }
        bitset_destroy(&testset);
    }
    
    srand(time(NULL));
    for (size_t test_it=0; test_it<1000; test_it++) { 
        size_t size = 1024;
        size_t pos = rand() % size;
        bitset_init(&testset, size);
        size_t index = pos / (8*sizeof(bitset_container_t));
        size_t rel_index = pos % (8*sizeof(bitset_container_t));
        bitset_container_t number = ((bitset_container_t) 1 << rel_index);

        int r = bitset_set_bit(&testset, pos);
        if (0 != r) {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed while setting the bit.");
        }

        CU_ASSERT_EQUAL_FATAL(1, bitset_get_bit(&testset, pos));

        bool correct = true;
        for (size_t i = 0; i<index+1; i++) {
            if (i == index) {
                correct = correct && (testset.container[i] == number);
            } else {
                correct = correct && (testset.container[i] == 0);
            }
        }
        if (correct == true) {
            CU_PASS("bitset_set_bit(<initialised bit set>,<position in range>): Successfully set only the desired bit.");
        } else {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed to set only the desired bit.");
        }
        bitset_destroy(&testset);
    }
}

// 3+1000
void test_bitset_set_all_bits(void) {
    bitset testset;

    // check for correct return value for NULL pointer as bit set
    CU_ASSERT_EQUAL_FATAL(-1, bitset_set_all_bits(NULL));

    // check for correct handling of a not properly initialised bit set
    testset.container = NULL;
    testset.size = 10;
    CU_ASSERT_EQUAL_FATAL(-1, bitset_set_all_bits(&testset));

    testset.container = malloc(10*sizeof(bitset_container_t));
    testset.size = 0;
    CU_ASSERT_EQUAL_FATAL(-1, bitset_set_all_bits(&testset));
    bitset_destroy(&testset);

    // check for setting all bits in the bit set (and nothing more)
    size_t size = 1024;
    for (size_t test_it=0; test_it<1000; test_it++) {
        bitset_init(&testset, size);
        int r = bitset_set_all_bits(&testset);
        if (0 != r) {
            CU_FAIL_FATAL("bitset_set_all_bits(<initialised bit set>): Failed while setting all bits.");
        }

        size_t last_index = (size-1) / (8*sizeof(bitset_container_t));
        size_t last_rel_index = (size-1) % (8*sizeof(bitset_container_t));
        size_t offset = 8*sizeof(bitset_container_t) - (last_rel_index+1);
        bitset_container_t number = ((bitset_container_t) (~0)) >> offset;

        bool correct = true;
        for (size_t i=0; i<last_index+1; i++) {
            if (i == last_index) {
                correct = correct && (testset.container[i] == number);
            } else {
                correct = correct && (testset.container[i] == ~0);
            }
        }
        if (correct == true) {
            CU_PASS("bitset_set_all_bits(<initialised bit set>): Successfully set all bits.");
        } else {
            CU_FAIL_FATAL("bitset_set_all_bits(<initialised bit set>): Failed setting all bits correctly.");
        }
        bitset_destroy(&testset);
    }
}

// 4+1000
void test_bitset_unset_bit(void) {
    bitset testset;

    // check for correct return value for NULL pointer as bit set
    CU_ASSERT_EQUAL_FATAL(-1, bitset_unset_bit(NULL, 0));

    // check for correct handling of a not properly initialised bit set
    testset.container = NULL;
    testset.size = 20;
    CU_ASSERT_EQUAL_FATAL(-1, bitset_unset_bit(&testset, 10));

    testset.container = malloc(10*sizeof(bitset_container_t));
    testset.size = 0;
    CU_ASSERT_EQUAL_FATAL(-1, bitset_unset_bit(&testset, 0));
    bitset_destroy(&testset);

    // check for correct handling of a position out of the range of the bit set
    bitset_init(&testset, 1024);
    CU_ASSERT_EQUAL_FATAL(-2, bitset_unset_bit(&testset, 1024));
    bitset_destroy(&testset);

    // check for setting only the desired bit
    {
        size_t size = 1024;
        size_t pos = size-1;
        bitset_init(&testset, size);
        bitset_set_all_bits(&testset);

        size_t index = pos / (8*sizeof(bitset_container_t));
        size_t rel_index = pos % (8*sizeof(bitset_container_t));
        bitset_container_t number = ~((bitset_container_t) 1 << rel_index);
        
        int r = bitset_unset_bit(&testset, pos);
        if (r != 0) {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed while setting the bit.");
        }

        CU_ASSERT_EQUAL_FATAL(0, bitset_get_bit(&testset, pos));

        bool correct = true;
        for (size_t i = 0; i<index+1; i++) {
            if (i == index) {
                correct = correct && (testset.container[i] == number);
            } else {
                correct = correct && (testset.container[i] == ~0);
            }
        }
        if (correct == true) {
            CU_PASS("bitset_set_bit(<initialised bit set>,<position in range>): Successfully set only the desired bit.");
        } else {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed to set only the desired bit.");
        }
        bitset_destroy(&testset);
    }
    
    {
        size_t size = 1024;
        size_t pos = 0;
        bitset_init(&testset, size);
        size_t index = pos / (8*sizeof(bitset_container_t));
        size_t rel_index = pos % (8*sizeof(bitset_container_t));
        bitset_container_t number = ((bitset_container_t) 1 << rel_index);

        int r = bitset_set_bit(&testset, pos);
        if (0 != r) {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed while setting the bit.");
        }

        CU_ASSERT_EQUAL_FATAL(1, bitset_get_bit(&testset, pos));

        bool correct = true;
        for (size_t i = 0; i<index+1; i++) {
            if (i == index) {
                correct = correct && (testset.container[i] == number);
            } else {
                correct = correct && (testset.container[i] == 0);
            }
        }
        if (correct == true) {
            CU_PASS("bitset_set_bit(<initialised bit set>,<position in range>): Successfully set only the desired bit.");
        } else {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed to set only the desired bit.");
        }
        bitset_destroy(&testset);
    }

    srand(time(NULL));
    for (size_t test_it=0; test_it<1000; test_it++) { 
        size_t size = 1024;
        size_t pos = rand() % size;
        bitset_init(&testset, size);
        size_t index = pos / (8*sizeof(bitset_container_t));
        size_t rel_index = pos % (8*sizeof(bitset_container_t));
        bitset_container_t number = ((bitset_container_t) 1 << rel_index);

        int r = bitset_set_bit(&testset, pos);
        if (0 != r) {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed while setting the bit.");
        }

        CU_ASSERT_EQUAL_FATAL(1, bitset_get_bit(&testset, pos));

        bool correct = true;
        for (size_t i = 0; i<index+1; i++) {
            if (i == index) {
                correct = correct && (testset.container[i] == number);
            } else {
                correct = correct && (testset.container[i] == 0);
            }
        }
        if (correct == true) {
            CU_PASS("bitset_set_bit(<initialised bit set>,<position in range>): Successfully set only the desired bit.");
        } else {
            CU_FAIL_FATAL("bitset_set_bit(<initialised bit set>,<position in range>): Failed to set only the desired bit.");
        }
        bitset_destroy(&testset);
    }
}

